import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss'],
})
export class LineChartComponent implements OnInit {
  public lineChartData: ChartDataSets[] = [];

  public lineChartLabels: Label[] = [];

  public lineChartOptions = {
    responsive: true,
  };

  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  public lineChartLegend = true;
  public lineChartPlugins = [];
  public lineChartType: ChartType = 'line';

  constructor(public dataService: DataService) {}

  ngOnInit() {
    this.dataService.getAllIssues();
    this.dataService.dataChange.subscribe((issues) => {
      this.lineChartLabels = issues.slice(0, 10).map((issue: any) => {
        return issue.title;
      });

      const data = issues.slice(0, 10).map((issue: any) => {
        return issue.number;
      });

      this.lineChartData = [{ data, label: 'number' }];
    });
  }
}
