import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Issue } from '../models/issue';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private readonly API_URL =
    'https://api.github.com/repos/angular/angular/issues';

  public dataChange: BehaviorSubject<Issue[]> = new BehaviorSubject<Issue[]>(
    []
  );

  private dialogData!: Issue;

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private httpClient: HttpClient) {}

  getDialogData(): Issue {
    return this.dialogData;
  }

  getAllIssues(): void {
    if (this.dataChange.value.length === 0) {
      this.loadingSubject.next(true);
      this.httpClient
        .get<Issue[]>(this.API_URL)
        .pipe(finalize(() => this.loadingSubject.next(false)))
        .subscribe(
          (data) => {
            this.dataChange.next(data);
          },
          (error: HttpErrorResponse) => {
            console.log(error.name + ' ' + error.message);
          }
        );
    }
  }

  addIssue(issue: Issue): void {
    this.dialogData = issue;
  }

  updateIssue(issue: Issue): void {
    this.dialogData = issue;
  }

  deleteIssue(id: number): void {
    console.log(id);
  }
}
