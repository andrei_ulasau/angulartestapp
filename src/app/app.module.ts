import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ThemeModule } from './theme/theme.module';
import { lightTheme } from './theme/light-theme';
import { darkTheme } from './theme/dark-theme';
import { BrowserModule } from '@angular/platform-browser';
import { DemoMaterialModule } from './material-module';
import { HttpClientModule } from '@angular/common/http';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { LineChartComponent } from './line-chart/line-chart.component';
import { MatTableComponent } from './mat-table/mat-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AddDialogComponent } from './mat-table/dialogs/add-dialog/add-dialog.component';
import { DataService } from './services/data.service';
import { EditDialogComponent } from './mat-table/dialogs/edit-dialog/edit-dialog.component';
import { DeleteDialogComponent } from './mat-table/dialogs/delete-dialog/delete-dialog.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    DemoMaterialModule,
    NgxChartsModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ThemeModule.forRoot({
      themes: [lightTheme, darkTheme],
      active: 'light',
    }),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
  ],
  declarations: [
    AppComponent,
    TopBarComponent,
    LineChartComponent,
    MatTableComponent,
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
  ],
  providers: [DataService],
  bootstrap: [AppComponent],
})
export class AppModule {}
