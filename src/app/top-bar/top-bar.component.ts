import { ApplicationRef, Component, OnInit, Input } from '@angular/core';
import { ThemeService } from '../theme/theme.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent implements OnInit {
  @Input()
  isChecked: boolean = false;
  isDarkModeActive: boolean =
    window.matchMedia &&
    window.matchMedia('(prefers-color-scheme: dark)').matches;

  constructor(
    private themeService: ThemeService,
    private ref: ApplicationRef
  ) {}

  ngOnInit() {
    // watch for changes of the preference
    window
      .matchMedia('(prefers-color-scheme: dark)')
      .addEventListener('change', (e) => {
        const isDark = e.matches;
        if (isDark) {
          this.themeService.setTheme('dark');
          this.isChecked = true;
        } else {
          this.themeService.setTheme('light');
          this.isChecked = false;
        }

        // trigger refresh of UI
        this.ref.tick();
      });

    if (this.isDarkModeActive) {
      this.themeService.setTheme('dark');
      this.isChecked = true;
    }

    const activeTheme = localStorage.getItem('theme');
    if (activeTheme === 'light') {
      this.themeService.setTheme('light');
      this.isChecked = false;
    }

    if (activeTheme === 'dark') {
      this.themeService.setTheme('dark');
      this.isChecked = true;
    }
  }

  toggle() {
    const activeTheme = this.themeService.getActiveTheme();
    if (activeTheme.name === 'light') {
      this.themeService.setTheme('dark');
      localStorage.setItem('theme', 'dark');
    }

    if (activeTheme.name === 'dark') {
      this.themeService.setTheme('light');
      localStorage.setItem('theme', 'light');
    }
  }
}
