import { Injectable, Inject, EventEmitter } from '@angular/core';
import { THEMES, ACTIVE_THEME, Theme } from './symbols';

@Injectable()
export class ThemeService {
  themeChange = new EventEmitter<Theme>();

  constructor(
    @Inject(THEMES) public themes: Theme[],
    @Inject(ACTIVE_THEME) public activeTheme: string
  ) {}

  getActiveTheme() {
    const activeTheme = this.themes.find((t) => t.name === this.activeTheme);
    if (!activeTheme) {
      throw new Error(`Theme not found: '${this.activeTheme}'`);
    }
    return activeTheme;
  }

  setTheme(name: string) {
    this.activeTheme = name;
    this.themeChange.emit(this.getActiveTheme());
  }
}
