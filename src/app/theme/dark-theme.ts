import { Theme } from "./symbols";

export const darkTheme: Theme = {
  name: "dark",
  properties: {
    "--background": "#000",
    "--primary": "#fff"
  }
};
