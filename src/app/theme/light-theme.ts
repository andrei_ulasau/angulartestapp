import { Theme } from "./symbols";

export const lightTheme: Theme = {
  name: "light",
  properties: {
    "--background": "#fff",
    "--primary": "#000"
  }
};
