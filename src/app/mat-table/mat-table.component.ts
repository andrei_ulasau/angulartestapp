import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AddDialogComponent } from './dialogs/add-dialog/add-dialog.component';
import { EditDialogComponent } from './dialogs/edit-dialog/edit-dialog.component';
import { DeleteDialogComponent } from './dialogs/delete-dialog/delete-dialog.component';
import { fromEvent } from 'rxjs';
import { ExampleDataSource } from './mat-table-datasource';

@Component({
  selector: 'app-mat-table',
  templateUrl: './mat-table.component.html',
  styleUrls: ['./mat-table.component.scss'],
})
export class MatTableComponent implements OnInit {
  displayedColumns = [
    'id',
    'title',
    'state',
    'url',
    'created_at',
    'updated_at',
    'actions',
  ];
  exampleDatabase!: DataService;
  dataSource!: ExampleDataSource;
  id!: number;

  constructor(
    private httpClient: HttpClient,
    private dialogService: MatDialog,
    private dataService: DataService
  ) {}

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  reload() {
    this.loadData();
  }

  openAddDialog() {
    const dialogRef = this.dialogService.open(AddDialogComponent, {
      data: { issue: {} },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        debugger;
        this.exampleDatabase.dataChange.value.push(
          this.dataService.getDialogData()
        );
        this.refreshTable();
      }
    });
  }

  startEdit(
    id: number,
    title: string,
    state: string,
    url: string,
    created_at: string,
    updated_at: string
  ) {
    this.id = id;
    const dialogRef = this.dialogService.open(EditDialogComponent, {
      data: {
        id: id,
        title: title,
        state: state,
        url: url,
        created_at: created_at,
        updated_at: updated_at,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.exampleDatabase.dataChange.value.findIndex(
          (issue) => issue.id === this.id
        );

        this.exampleDatabase.dataChange.value[
          foundIndex
        ] = this.dataService.getDialogData();

        this.refreshTable();
      }
    });
  }

  deleteItem(id: number, title: string, state: string, url: string) {
    this.id = id;
    const dialogRef = this.dialogService.open(DeleteDialogComponent, {
      data: { id: id, title: title, state: state, url: url },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.exampleDatabase.dataChange.value.findIndex(
          (issue) => issue.id === this.id
        );

        this.exampleDatabase.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  private loadData() {
    this.exampleDatabase = new DataService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );

    fromEvent(this.filter.nativeElement, 'keyup').subscribe(() => {
      if (!this.dataSource) {
        return;
      }
      this.dataSource.filter = this.filter.nativeElement.value;
    });
  }
}
